//
//  RxCollector.swift
//  MoScannerTests
//
//  Created by Fabricio Rodriguez on 7/3/18.
//  Copyright © 2018 Fabricio Rodriguez. All rights reserved.
//

import Foundation
@testable import MoScanner
import RxSwift

class RxCollector<T> {
    
    var disposeBag: DisposeBag = DisposeBag()
    
    //last position in the array will be the last result
    var toArray: [T] = [T]()
    
    var lastResult: T? {
        return toArray.last
    }
    
    var firstResult: T? {
        return toArray.first
    }
    
    func collect(from observable: Observable<T>) -> RxCollector {
        observable.asObservable()
            .subscribe(onNext: { (result) in
                self.toArray.append(result)
            }).disposed(by: disposeBag)
        return self
    }
}
