//
//  DeviceScannerViewControllerTests.swift
//  MoScannerTests
//
//  Created by Fabricio Rodriguez on 7/3/18.
//  Copyright © 2018 Fabricio Rodriguez. All rights reserved.
//

import XCTest
@testable import MoScanner
import RxBluetoothKit
import RxSwift
import RxCocoa

class DeviceScannerViewControllerTests: XCTestCase {
    
    var viewModel: DeviceScannerViewModel!
    var bluetoothService: BluetoothServiceMock!
    var viewController: DeviceScannerViewController?
    let testDevice = DeviceMock(localname: "testName", uuid: "ADFDG-DFAD", manufacturerData: "0x235", connectable: true)
    
    override func setUp() {
        super.setUp()
        bluetoothService = BluetoothServiceMock(state: .poweredOn)
        viewModel = DeviceScannerViewModel(bluetoothService: bluetoothService)
        viewController = DeviceScannerViewController.instantiate(with: viewModel)
        let navigationController = UINavigationController(rootViewController: viewController!)
        navigationController.loadView()
        navigationController.viewDidLoad()
        viewController?.loadView()
        viewController?.viewDidLoad()
    }
    
    func test_show_stop() {
        viewModel.scanTapped()
        XCTAssertTrue(viewController!.navigationItem.rightBarButtonItem!.title == NSLocalizedString(Constants.LocalizedKeys.stop, comment: ""))
    }
    
    func test_show_scan_after_stop() {
        viewModel.scanTapped()
        viewModel.scanTapped()
        XCTAssertEqual(viewController!.navigationItem.rightBarButtonItem!.title, NSLocalizedString(Constants.LocalizedKeys.scan, comment: ""))
    }
    
    func test_show_message_power_off() {
        bluetoothService.changeStateTo(.poweredOff)
        XCTAssertTrue(!viewController!.messageLabel.text!.isEmpty)
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
}
