//
//  DeviceScannerViewModelTests.swift
//  MoScannerTests
//
//  Created by Fabricio Rodriguez on 7/2/18.
//  Copyright © 2018 Fabricio Rodriguez. All rights reserved.
//
@testable import MoScanner
import XCTest
import RxBluetoothKit
import RxSwift
import RxCocoa

class DeviceScannerViewModelTests: XCTestCase {
    
    var viewModel: DeviceScannerViewModel!
    var bluetoothService: BluetoothServiceMock!
    let testDevice = DeviceMock(localname: "testName", uuid: "ADFDG-DFAD", manufacturerData: "0x235", connectable: true)
    
    override func setUp() {
        super.setUp()
        bluetoothService = BluetoothServiceMock(state: .poweredOn)
        viewModel = DeviceScannerViewModel(bluetoothService: bluetoothService)
    }
    
    func test_inputsProtocol_shouldNotBeNil() {
        //assemble
        let inputs = viewModel.inputs
        
        //assert
        XCTAssertNotNil(inputs)
    }
    
    func test_outputsProtocol_shouldNotBeNil() {
        //assemble
        let outputs = viewModel.outputs
        
        //assert
        XCTAssertNotNil(outputs)
    }
    
    func test_show_message() {
        let collector = RxCollector<String?>().collect(from: viewModel.outputs.message)
        bluetoothService.changeStateTo(.poweredOff)
        XCTAssertTrue(collector.lastResult != nil && !collector.lastResult!!.isEmpty)
    }
    
    func test_discover_device() {
        let collector = RxCollector<[Device]>().collect(from: viewModel.outputs.devices.asObservable())
        bluetoothService.add(device: testDevice)
        XCTAssertTrue(collector.lastResult!.contains { $0.uuid == testDevice.uuid })
    }
    
    func test_discover_multiple_devices() {
        let collector = RxCollector<[Device]>().collect(from: viewModel.outputs.devices.asObservable())
        let secondDevice = DeviceMock(localname: "testName", uuid: "ADFCDGAG-DFAD", manufacturerData: "0x235", connectable: true)
        bluetoothService.add(device: testDevice)
        bluetoothService.add(device: secondDevice)
        XCTAssertTrue(collector.lastResult!.contains { $0.uuid == testDevice.uuid } && collector.lastResult!.contains { $0.uuid == secondDevice.uuid })
    }
    
    func test_not_scanning() {
        let collector = RxCollector<Bool>().collect(from: viewModel.isScanning.asObservable())
        XCTAssertFalse(collector.lastResult!)
    }
    
    func test_is_scanning() {
        let collector = RxCollector<Bool>().collect(from: viewModel.isScanning.asObservable())
        viewModel.scanTapped()
        XCTAssertTrue(collector.lastResult!)
    }
    
    func test_stop_after_scan() {
        let collector = RxCollector<Bool>().collect(from: viewModel.isScanning.asObservable())
        viewModel.scanTapped()
        viewModel.scanTapped()
        XCTAssertFalse(collector.lastResult!)
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
}
