//
//  DeviceDetailViewControllerTests.swift
//  MoScannerTests
//
//  Created by Fabricio Rodriguez on 7/3/18.
//  Copyright © 2018 Fabricio Rodriguez. All rights reserved.
//

import XCTest
@testable import MoScanner
import RxBluetoothKit
import RxSwift
import RxCocoa

class DeviceDetailViewControllerTests: XCTestCase {
    
    var viewModel: DeviceDetailViewModel!
    var bluetoothService: BluetoothServiceMock!
    var viewController: DeviceDetailViewController?
    let testDevice = DeviceMock(localname: "testName", uuid: "ADFDG-DFAD", manufacturerData: "0x235", connectable: true)
    
    override func setUp() {
        super.setUp()
        viewModel = DeviceDetailViewModel(device: testDevice)
        viewController = DeviceDetailViewController.instantiate(with: viewModel)
        let navigationController = UINavigationController(rootViewController: viewController!)
        navigationController.loadView()
        navigationController.viewDidLoad()
        viewController?.loadView()
        viewController?.viewDidLoad()
    }
    
    func test_local_name() {
        XCTAssertEqual(viewController?.localNameLabel.text, testDevice.localName)
    }
    
    func test_manufacturer_data() {
        XCTAssertEqual(viewController?.manufacturerDataLabel.text, testDevice.manufacturerData)
    }
    
    func test_uuid() {
        XCTAssertEqual(viewController?.uuidLabel.text, testDevice.uuid)
    }
    
}
