//
//  DeviceDetailViewModelTests.swift
//  MoScannerTests
//
//  Created by Fabricio Rodriguez on 7/3/18.
//  Copyright © 2018 Fabricio Rodriguez. All rights reserved.
//

@testable import MoScanner
import XCTest
import RxBluetoothKit
import RxSwift
import RxCocoa

class DeviceDetailViewModelTests: XCTestCase {
    
    var viewModel: DeviceDetailViewModel!
    let testDevice = DeviceMock(localname: "testName", uuid: "ADFDG-DFAD", manufacturerData: "0x235", connectable: true)
    let testCharacteristic = DeviceCharacteristicMock(uuid: "0x234", accessLevel: AccessLevel.readWrite, value: "Ble")
    
    override func setUp() {
        super.setUp()
        viewModel = DeviceDetailViewModel(device: testDevice)
    }
    
    func test_inputsProtocol_shouldNotBeNil() {
        //assemble
        let inputs = viewModel.inputs
        
        //assert
        XCTAssertNotNil(inputs)
    }
    
    func test_outputsProtocol_shouldNotBeNil() {
        //assemble
        let outputs = viewModel.outputs
        
        //assert
        XCTAssertNotNil(outputs)
    }
    
    func test_local_name() {
        let collector = RxCollector<String?>().collect(from: viewModel.outputs.localName.asObservable())
        XCTAssertEqual(testDevice.localName, collector.lastResult)
    }
    
    func test_manufacturer_data() {
        let collector = RxCollector<String?>().collect(from: viewModel.outputs.manufacturerData.asObservable())
        XCTAssertEqual(testDevice.manufacturerData, collector.lastResult)
    }
    
    func test_uuid() {
        let collector = RxCollector<String?>().collect(from: viewModel.outputs.uuid.asObservable())
        XCTAssertEqual(testDevice.uuid, collector.lastResult)
    }
    
    func test_characteristics() {
        let collector = RxCollector<[DeviceCharacteristic]>().collect(from: viewModel.outputs.characteristics.asObservable())
        testDevice.add(testCharacteristic)
        XCTAssertTrue(testCharacteristic.uuid == collector.lastResult?.first?.uuid)
    }
    
    override func tearDown() {
        super.tearDown()
    }
}
