//
//  DeviceCharacteristicMock.swift
//  MoScannerTests
//
//  Created by Fabricio Rodriguez on 7/3/18.
//  Copyright © 2018 Fabricio Rodriguez. All rights reserved.
//

import Foundation
@testable import MoScanner
import RxBluetoothKit
import RxSwift
import RxCocoa

class DeviceCharacteristicMock: DeviceCharacteristic {
    
    var uuid: String {
        return _uuid
    }
    
    var accessLevel: AccessLevel {
        return _accessLevel
    }
    
    var value: String {
        return _value
    }
    
    private var _uuid: String
    private var _accessLevel: AccessLevel
    private var _value: String
    
    init(uuid: String, accessLevel: AccessLevel, value: String) {
        self._uuid = uuid
        self._accessLevel = accessLevel
        self._value = value
    }
    
}
