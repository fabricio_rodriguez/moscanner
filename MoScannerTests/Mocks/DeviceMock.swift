//
//  DeviceMock.swift
//  MoScannerTests
//
//  Created by Fabricio Rodriguez on 7/3/18.
//  Copyright © 2018 Fabricio Rodriguez. All rights reserved.
//

import Foundation
@testable import MoScanner
import RxBluetoothKit
import RxSwift
import RxCocoa

class DeviceMock: Device {
    
    var localName: String {
        return _localname
    }
    
    var uuid: String {
        return _uuid
    }
    
    var manufacturerData: String {
        return _manufacturerData
    }
    
    var connectable: Bool {
        return _connectable
    }
    
    func observeCharacteristics() -> Observable<DeviceCharacteristic> {
        return _characteristics
    }
    
    private var _localname: String
    private var _uuid: String
    private var _manufacturerData: String
    private var _connectable: Bool
    private var _characteristics: PublishSubject<DeviceCharacteristic>
    
    init(localname: String, uuid: String, manufacturerData: String, connectable: Bool) {
        self._localname = localname
        self._uuid = uuid
        self._manufacturerData = manufacturerData
        self._connectable = connectable
        self._characteristics = PublishSubject<DeviceCharacteristic>()
    }
    
    func add(_ characteristic: DeviceCharacteristic) {
        self._characteristics.onNext(characteristic)
    }
}
