//
//  BluetoothServiceMock.swift
//  MoScannerTests
//
//  Created by Fabricio Rodriguez on 7/2/18.
//  Copyright © 2018 Fabricio Rodriguez. All rights reserved.
//

import Foundation
@testable import MoScanner
import RxBluetoothKit
import RxSwift
import RxCocoa

class BluetoothServiceMock: BluetoothService {
    
    var state: BluetoothState {
        return _state.value
    }
    
    func observeState() -> Observable<BluetoothState> {
        return _state.asObservable()
    }
    
    func scanPeripherals() -> Observable<Device> {
        return _devices
    }
    
    private var _state: BehaviorRelay<BluetoothState>
    private var _devices: PublishSubject<Device>
    
    init(state: BluetoothState) {
        self._state = BehaviorRelay<BluetoothState>(value: state)
        self._devices = PublishSubject<Device>()
    }
    
    func changeStateTo(_ state: BluetoothState) {
        self._state.accept(state)
    }
    
    func add(device: Device) {
        self._devices.onNext(device)
    }
}
