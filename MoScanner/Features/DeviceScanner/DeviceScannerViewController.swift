//
//  DeviceScannerViewController.swift
//  MoScanner
//
//  Created by Fabricio Rodriguez on 7/1/18.
//  Copyright © 2018 Fabricio Rodriguez. All rights reserved.
//

import UIKit
import RxSwift
import RxBluetoothKit

class DeviceScannerViewController: UIViewController, UITableViewDelegate, StoryboardInitializable {
    
    // MARK: - Outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var messageLabel: UILabel!
    
    // MARK: - View Model
    fileprivate var deviceScannerViewModel: DeviceScannerViewModelType!
    var viewModel: ViewModelType? {
        get {
            return deviceScannerViewModel
        }
        set {
            deviceScannerViewModel = newValue as? DeviceScannerViewModelType
        }
    }
    // MARK: - Properties
    var disposeBag  = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setDefaultStates()
        createBindings()
    }
}

extension DeviceScannerViewController {
    // MARK: - Bindings
    private func createBindings() {
        navigationItem.rightBarButtonItem?
            .rx
            .tap
            .subscribe(onNext: {[weak self] _ in
                guard let strongSelf = self else { return }
                strongSelf.deviceScannerViewModel.inputs.scanTapped()
            }).disposed(by: disposeBag)
        
        deviceScannerViewModel.outputs
            .isScanning
            .subscribe(onNext: {[weak self] isScanning in
                guard let strongSelf = self else { return }
                let buttonTitle = isScanning ? strongSelf.deviceScannerViewModel.outputs.stopButtonTitle : strongSelf.deviceScannerViewModel.outputs.scanButtonTitle
                let navigationTitle = isScanning ? strongSelf.deviceScannerViewModel.outputs.scanningTitle : strongSelf.deviceScannerViewModel.outputs.nativationTitle
                strongSelf.navigationItem.rightBarButtonItem?.title = buttonTitle
                strongSelf.navigationItem.title = navigationTitle
            })
            .disposed(by: disposeBag)
        
        deviceScannerViewModel.outputs
            .message
            .subscribe(onNext: {[weak self] message in
                guard let strongSelf = self else { return }
                strongSelf.messageLabel.isHidden = false
                strongSelf.messageLabel.text = message
            })
            .disposed(by: disposeBag)
        
        deviceScannerViewModel.outputs
            .devices
            .bind(to: tableView.rx.items) {[weak self] (tableView, row, device) in
                let indexPath = IndexPath(row: row, section: 0)
                guard let cell = tableView.dequeueReusableCell(withIdentifier: DeviceTableViewCell.reuseIdentifier, for: indexPath) as? DeviceTableViewCell else {
                    preconditionFailure(Constants.Error.cellInitFailed)
                }
                cell.nameLabel.text = device.localName
                cell.connectButton.setTitle(self?.deviceScannerViewModel.outputs.connectButtonTitle, for: .normal)
                if device.connectable {
                    cell.connectableLabel.text = self?.deviceScannerViewModel.outputs.connectableTitle
                    cell.connectButton.isHidden = false
                    cell.connectButton.addTarget(self, action: #selector(self?.connectAction(_:)), for: .touchUpInside)
                } else {
                    cell.connectableLabel.text = self?.deviceScannerViewModel.outputs.nonConnectableTitle
                    cell.connectButton.isHidden = true
                }
                return cell
            }
            .disposed(by: disposeBag)
    }
}

extension DeviceScannerViewController {
    // MARK: - Initializers
    private func setDefaultStates() {
        navigationItem.title = deviceScannerViewModel.outputs.nativationTitle
        tableView.register(DeviceTableViewCell.nib, forCellReuseIdentifier: DeviceTableViewCell.reuseIdentifier)
        tableView.tableFooterView = UIView()
        let scanBarButton = UIBarButtonItem.init(title: deviceScannerViewModel.outputs.scanButtonTitle, style: .plain, target: nil, action: nil)
        navigationItem.rightBarButtonItem = scanBarButton
    }
    
    @objc private func connectAction(_ button: UIButton) {
        guard let cell = button.superview?.superview as? UITableViewCell,
            let indexPath = tableView.indexPath(for: cell)
            else {
                return
        }
        let device = self.deviceScannerViewModel.outputs.devices.value[indexPath.row]
        let deviceDetailViewModel = DeviceDetailViewModel(device: device)
        let deviceDetailViewController = DeviceDetailViewController.instantiate(with: deviceDetailViewModel)
        navigationController?.pushViewController(deviceDetailViewController, animated: true)
    }
}
