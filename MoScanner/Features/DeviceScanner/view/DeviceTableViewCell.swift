//
//  DeviceTableViewCell.swift
//  MoScanner
//
//  Created by Fabricio Rodriguez on 7/1/18.
//  Copyright © 2018 Fabricio Rodriguez. All rights reserved.
//

import UIKit

class DeviceTableViewCell: UITableViewCell, NibInitializable {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var connectableLabel: UILabel!
    @IBOutlet weak var connectButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func connectTouchUpInside(_ sender: Any) {
    }
}
