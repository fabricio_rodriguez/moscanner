//
//  DeviceScannerViewModel.swift
//  MoScanner
//
//  Created by Fabricio Rodriguez on 7/1/18.
//  Copyright © 2018 Fabricio Rodriguez. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import CoreBluetooth

protocol DeviceScannerViewModelOutputs {
    var isScanning: BehaviorRelay<Bool> { get }
    var devices: BehaviorRelay<[Device]> { get }
    var scanButtonTitle: String { get }
    var stopButtonTitle: String { get }
    var connectButtonTitle: String { get }
    var connectableTitle: String { get }
    var nonConnectableTitle: String { get }
    var nativationTitle: String { get }
    var scanningTitle: String { get }
    var message: BehaviorSubject<String?> { get }
}

protocol DeviceScannerViewModelInputs {
    func initialScan()
    func scanTapped()
}

protocol DeviceScannerViewModelType: ViewModelType {
    var inputs: DeviceScannerViewModelInputs { get }
    var outputs: DeviceScannerViewModelOutputs { get }
}

class DeviceScannerViewModel: DeviceScannerViewModelType, DeviceScannerViewModelOutputs, DeviceScannerViewModelInputs {
    
    var outputs: DeviceScannerViewModelOutputs { return self }
    var inputs: DeviceScannerViewModelInputs { return self }
    
    // MARK: - Outputs
    var isScanning: BehaviorRelay<Bool> = BehaviorRelay<Bool>(value: false)
    var devices: BehaviorRelay<[Device]> = BehaviorRelay<[Device]>(value: [])
    var message: BehaviorSubject<String?> = BehaviorSubject<String?>(value: nil)
    
    var scanButtonTitle: String {
        return NSLocalizedString(Constants.LocalizedKeys.scan, comment: "")
    }
    
    var stopButtonTitle: String {
        return NSLocalizedString(Constants.LocalizedKeys.stop, comment: "")
    }
    
    var connectableTitle: String {
        return NSLocalizedString(Constants.LocalizedKeys.connectable, comment: "")
    }
    
    var nonConnectableTitle: String {
        return NSLocalizedString(Constants.LocalizedKeys.nonConnectable, comment: "")
    }
    
    var connectButtonTitle: String {
        return NSLocalizedString(Constants.LocalizedKeys.connect, comment: "")
    }
    
    var nativationTitle: String {
        return NSLocalizedString(Constants.LocalizedKeys.navigationTitle, comment: "")
    }
    
    var scanningTitle: String {
        return NSLocalizedString(Constants.LocalizedKeys.scanningTitle, comment: "")
    }
    
    private var enableBluetoothMessage: String {
        return NSLocalizedString(Constants.LocalizedKeys.enableBluetooth, comment: "")
    }
    
    private var unsupportedMessage: String {
        return NSLocalizedString(Constants.LocalizedKeys.bluetoothUnsupported, comment: "")
    }
    
    //private properties
    private var bluetoothService: BluetoothService
    
    private var scanningDisposable: Disposable?
    private var stateDisposable: Disposable?
    private let timerQueue = DispatchQueue(label: "com.test.moScanner")
    
    // MARK: - Initializer
    init(bluetoothService: BluetoothService = BluetoothServiceWrapper()) {
        self.bluetoothService = bluetoothService
        observeState()
        initialScan()
    }
    
    private func observeState() {
        stateDisposable = bluetoothService
            .observeState()
            .startWith(bluetoothService.state)
            .subscribe(onNext: { state in
                switch state {
                case .poweredOn:
                    self.message.onNext(nil)
                case .poweredOff, .unauthorized:
                    self.stopScan()
                    self.devices.accept([])
                    self.message.onNext(self.enableBluetoothMessage)
                case .unsupported:
                    self.stopScan()
                    self.devices.accept([])
                    self.message.onNext(self.unsupportedMessage)
                default:
                    //resetting and unknown cases, update is inminent
                    break
                }
            })
    }
    
    private func stopScan() {
        scanningDisposable?.dispose()
        isScanning.accept(false)
    }
    
    private func startScan() {
        scanningDisposable?.dispose()
        scanningDisposable = bluetoothService
            .scanPeripherals()
            .filter({ p1 -> Bool in
                //skip duplicated after subscription changes.
                return !self.devices.value.contains(where: { p2 -> Bool in
                    return p1.uuid == p2.uuid
                })
            })
            .subscribe(onNext: { device in
                var dict = self.devices.value
                dict.append(device)
                self.devices.accept(dict)
            })
    }

    // MARK: - Inputs
    func initialScan() {
        let scheduler = ConcurrentDispatchQueueScheduler(queue: timerQueue)
        //this will make a initial scan during 1 second
        //disposing the observable is not necessary to stop updates, it will be automatically disposed after 1 second.
        scanningDisposable = bluetoothService
            .observeState()
            .startWith(bluetoothService.state)
            .filter { $0 == .poweredOn }
            .timeout(1, scheduler: scheduler)
            .take(1)
            .flatMap { _  in
                self.bluetoothService.scanPeripherals()
            }
            .filter({ p1 -> Bool in
                //skip duplicated after subscription changes.
                return !self.devices.value.contains(where: { p2 -> Bool in
                    return p1.uuid == p2.uuid
                })
            })
            .take(2.0, scheduler: scheduler)
            .subscribe(onNext: { device in
                var dict = self.devices.value
                dict.append(device)
                self.devices.accept(dict)
            })
    }
    
    func scanTapped() {
        if isScanning.value {
            stopScan()
        } else if bluetoothService.state == .poweredOn {
            isScanning.accept(true)
            startScan()
        }
    }
}
