//
//  CharacteristicTableViewCell.swift
//  MoScanner
//
//  Created by Fabricio Rodriguez on 7/1/18.
//  Copyright © 2018 Fabricio Rodriguez. All rights reserved.
//

import UIKit
import RxBluetoothKit

class CharacteristicTableViewCell: UITableViewCell, NibInitializable {
    
    @IBOutlet weak var uuidLabel: UILabel!
    @IBOutlet weak var accessLevelLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}

extension CharacteristicTableViewCell {
    func configureWith(characteristic: DeviceCharacteristic) {
        uuidLabel.text = characteristic.uuid
        accessLevelLabel.text = characteristic.accessLevel.rawValue
        valueLabel.text = characteristic.value
    }
}
