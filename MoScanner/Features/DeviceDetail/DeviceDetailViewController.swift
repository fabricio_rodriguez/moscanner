//
//  DeviceDetailViewController.swift
//  MoScanner
//
//  Created by Fabricio Rodriguez on 7/1/18.
//  Copyright © 2018 Fabricio Rodriguez. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class DeviceDetailViewController: UIViewController, StoryboardInitializable {
    
    @IBOutlet weak var localNameLabel: UILabel!
    @IBOutlet weak var manufacturerDataLabel: UILabel!
    @IBOutlet weak var uuidLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    static var storyboardName: String {
        return "DeviceDetail"
    }
    
    // MARK: - View Model
    fileprivate var deviceDetailViewModel: DeviceDetailViewModelType!
    var viewModel: ViewModelType? {
        get {
            return deviceDetailViewModel
        }
        set {
            deviceDetailViewModel = newValue as? DeviceDetailViewModelType
        }
    }
    private var disposeBag  = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setDefaultStates()
        createBindings()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        deviceDetailViewModel.inputs.disconnect()
    }
}

extension DeviceDetailViewController {
    func setDefaultStates() {
        tableView.register(CharacteristicTableViewCell.nib, forCellReuseIdentifier: CharacteristicTableViewCell.reuseIdentifier)
        tableView.tableFooterView = UIView()
    }
}

extension DeviceDetailViewController {
    // MARK: - Bindings
    func createBindings() {
        deviceDetailViewModel
            .outputs
            .localName.subscribe(onNext: {[weak self] localName in
                guard let strongSelf = self else { return }
                strongSelf.localNameLabel.text = localName })
            .disposed(by: disposeBag)
        
        deviceDetailViewModel
            .outputs
            .uuid
            .subscribe(onNext: {[weak self] uuid in
                guard let strongSelf = self else { return }
                strongSelf.uuidLabel.text = uuid })
            .disposed(by: disposeBag)
        
        deviceDetailViewModel
            .outputs
            .manufacturerData
            .subscribe(onNext: {[weak self] manufacturerData in
                guard let strongSelf = self else { return }
                strongSelf.manufacturerDataLabel.text = manufacturerData })
            .disposed(by: disposeBag)
        
        deviceDetailViewModel
            .outputs
            .characteristics
            .bind(to: self.tableView.rx.items) {(tableView, row, characteristic) in
                let indexPath = IndexPath(row: row, section: 0)
                guard let cell = tableView.dequeueReusableCell(withIdentifier: CharacteristicTableViewCell.reuseIdentifier, for: indexPath) as? CharacteristicTableViewCell else {
                    preconditionFailure(Constants.Error.cellInitFailed)
                }
                cell.configureWith(characteristic: characteristic)
                return cell
        }.disposed(by: disposeBag)
    }
}
