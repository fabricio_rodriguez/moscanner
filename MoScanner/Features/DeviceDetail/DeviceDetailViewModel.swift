//
//  DeviceDetailViewModel.swift
//  MoScanner
//
//  Created by Fabricio Rodriguez on 7/1/18.
//  Copyright © 2018 Fabricio Rodriguez. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RxBluetoothKit

protocol DeviceDetailViewModelOutputs {
    var characteristics: BehaviorRelay<[DeviceCharacteristic]> { get }
    var localName: BehaviorSubject<String?> { get }
    var manufacturerData: BehaviorSubject<String?> { get }
    var uuid: BehaviorSubject<String?> { get }
}

protocol DeviceDetailViewModelInputs {
    func disconnect()
}

protocol DeviceDetailViewModelType: ViewModelType {
    var inputs: DeviceDetailViewModelInputs { get }
    var outputs: DeviceDetailViewModelOutputs { get }
}

class DeviceDetailViewModel: DeviceDetailViewModelType, DeviceDetailViewModelInputs, DeviceDetailViewModelOutputs {
    
    var outputs: DeviceDetailViewModelOutputs { return self }
    var inputs: DeviceDetailViewModelInputs { return self }
    
    // MARK: - Outputs
    var characteristics = BehaviorRelay<[DeviceCharacteristic]>(value: [])
    var localName = BehaviorSubject<String?>(value: nil)
    var manufacturerData = BehaviorSubject<String?>(value: nil)
    var uuid = BehaviorSubject<String?>(value: nil)
    
    private var connectionDisposable: Disposable?
    private var device: Device
    
    // MARK: - Initializer
    init( device: Device ) {
        self.device = device
        self.propagatePeripheralData()
        self.discoverCharacteristics()
    }
    
    private func propagatePeripheralData() {
        localName.onNext(device.localName)
        uuid.onNext(device.uuid)
        manufacturerData.onNext(device.manufacturerData)
    }
    
    private func discoverCharacteristics() {
        connectionDisposable = device
            .observeCharacteristics()
            .subscribe(onNext: { characteristic in
                var dict = self.characteristics.value
                dict.append(characteristic)
                self.characteristics.accept(dict)
            })
    }
    
    // MARK: - Inputs
    func disconnect() {
        connectionDisposable?.dispose()
    }
}
