//
//  Constants.swift
//  MoScanner
//
//  Created by Fabricio Rodriguez on 7/1/18.
//  Copyright © 2018 Fabricio Rodriguez. All rights reserved.
//

import Foundation

struct Constants {
    struct Error {
        static let storyboardInitializationFailed = "Failed to initalize view controller from storyboard"
        static let cellInitFailed = "Cell cannot be constructed and/or dequeued"
    }
    
    struct LocalizedKeys {
        static let scan = "SCAN"
        static let stop = "STOP"
        static let enableBluetooth = "ENABLE_BLUETOOTH"
        static let noBluetoothDevices = "NO_DEVICES"
        static let bluetoothUnsupported = "UNSUPPORTED"
        static let connectable = "CONNECTABLE"
        static let nonConnectable = "NONCONNECTABLE"
        static let connect = "CONNECT"
        static let unknown = "UNKNOWN"
        static let navigationTitle = "NAVIGATION_TITLE"
        static let scanningTitle = "SCANNING"
    }
}
