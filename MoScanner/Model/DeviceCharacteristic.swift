//
//  DeviceCharacteristic.swift
//  MoScanner
//
//  Created by Fabricio Rodriguez on 7/2/18.
//  Copyright © 2018 Fabricio Rodriguez. All rights reserved.
//

import Foundation
import RxBluetoothKit

protocol DeviceCharacteristic {
    var uuid: String { get }
    var accessLevel: AccessLevel { get }
    var value: String { get }
}

class DeviceCharacteristicWrapper: DeviceCharacteristic {
    
    var uuid: String {
        return characteristic.uuid.uuidString
    }
    
    var accessLevel: AccessLevel {
        var level: AccessLevel = .read
        if characteristic.characteristic.properties.isSuperset(of: [.write, .read, .writeWithoutResponse]) ||
            characteristic.characteristic.properties.isSuperset(of: [.write, .read]) {
            level = .readWrite
        } else if characteristic.characteristic.properties.contains(.write) {
            level = .write
        }
        return level
    }
    
    var value: String {
        guard let valueData = characteristic.value,
            let valueString = String(data: valueData, encoding: .utf8) else {
            return ""
        }
        return valueString
    }
    
    private var characteristic: Characteristic
    
    init(characteristic: Characteristic) {
        self.characteristic = characteristic
    }
}
