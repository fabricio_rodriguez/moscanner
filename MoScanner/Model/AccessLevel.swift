//
//  AccessLevel.swift
//  MoScanner
//
//  Created by Fabricio Rodriguez on 7/2/18.
//  Copyright © 2018 Fabricio Rodriguez. All rights reserved.
//

import Foundation

enum AccessLevel: String {
    case read = "Read"
    case write = "Write"
    case readWrite = "Read/Write"
}
