//
//  Device.swift
//  MoScanner
//
//  Created by Fabricio Rodriguez on 7/2/18.
//  Copyright © 2018 Fabricio Rodriguez. All rights reserved.
//

import Foundation
import RxSwift
import RxBluetoothKit

protocol Device {
    var localName: String { get }
    var uuid: String { get }
    var manufacturerData: String { get }
    var connectable: Bool { get }
    func observeCharacteristics() -> Observable<DeviceCharacteristic>
}

class DeviceWrapper: Device {
    
    var localName: String {
        return peripheral.advertisementData.localName ?? peripheral.peripheral.name ?? NSLocalizedString(Constants.LocalizedKeys.unknown, comment: "")
    }
    
    var uuid: String {
        return peripheral.peripheral.identifier.uuidString
    }
    
    var manufacturerData: String {
        guard let manData = peripheral.advertisementData.manufacturerData else {
            return ""
        }
        return  manData.map { String(format: "%02x", $0) }.joined(separator: "")
    }
    
    var connectable: Bool {
        return peripheral.advertisementData.isConnectable ?? false
    }
    
    private var peripheral: ScannedPeripheral
    
    init(peripheral: ScannedPeripheral) {
        self.peripheral = peripheral
    }
    
    func observeCharacteristics() -> Observable<DeviceCharacteristic> {
        return peripheral.peripheral.establishConnection()
            .flatMap { $0.discoverServices(nil) }
            .flatMap { Observable.from($0) }
            .flatMap { $0.discoverCharacteristics(nil) }
            .flatMap { Observable.from($0) }
            .filter { $0.characteristic.properties.contains(.read) }
            .flatMap { $0.readValue() }
            .flatMap { Observable.from(optional: DeviceCharacteristicWrapper(characteristic: $0)) }
    }
}
