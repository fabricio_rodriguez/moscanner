//
//  BluetoothManager.swift
//  MoScanner
//
//  Created by Fabricio Rodriguez on 7/1/18.
//  Copyright © 2018 Fabricio Rodriguez. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RxBluetoothKit
import CoreBluetooth

protocol BluetoothService: class {
    var state: BluetoothState { get }
    func observeState() -> Observable<BluetoothState>
    func scanPeripherals() -> Observable<Device>
}

class BluetoothServiceWrapper: BluetoothService {
    
    private var centralManager: CentralManager
    
    var state: BluetoothState {
        return centralManager.state
    }
    
    init(centralManager: CentralManager = CentralManager(queue: .main, options: nil)) {
        self.centralManager = centralManager
    }
    
    func observeState() -> Observable<BluetoothState> {
        return centralManager.observeState()
    }
    
    func scanPeripherals() -> Observable<Device> {
        return centralManager
            .scanForPeripherals(withServices: nil)
            .flatMap { Observable.from(optional: DeviceWrapper(peripheral: $0)) }
    }
}
