//
//  NibInitializable.swift
//  MoScanner
//
//  Created by Fabricio Rodriguez on 7/1/18.
//  Copyright © 2018 Fabricio Rodriguez. All rights reserved.
//

import Foundation
import UIKit

protocol NibInitializable {
    static var reuseIdentifier: String { get }
    static var nib: UINib { get }
}

extension NibInitializable where Self: UITableViewCell {
    static var reuseIdentifier: String {
        return String(describing: self)
    }
    
    static var nib: UINib {
        return UINib(nibName: String(describing: self), bundle: nil)
    }
}
