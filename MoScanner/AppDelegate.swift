//
//  AppDelegate.swift
//  MoScanner
//
//  Created by Fabricio Rodriguez on 6/30/18.
//  Copyright © 2018 Fabricio Rodriguez. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        startMainViewController()
        return true
    }

    private func startMainViewController() {
        let viewModel = DeviceScannerViewModel()
        let scanViewController = DeviceScannerViewController.instantiate(with: viewModel)
        let navigationController = UINavigationController.init(rootViewController: scanViewController)
        window = UIWindow()
        window?.rootViewController = navigationController
        window?.makeKeyAndVisible()
    }
}
