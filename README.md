# MoScanner #

This repository contains the code of MoScanner. 

# Instructions #

Clone this repository and run pod install in the main directory.

```pod install```

Select a development team to run in a device and you are ready to go.